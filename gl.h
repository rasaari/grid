#ifndef GL_H
#define GL_H

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "types.h"
#include <SDL/SDL_image.h>

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

#define VIEW_ANGLE 80.0f

#define COLOR_BLACK 	0.f, 0.f, 0.f
#define COLOR_RED   	1.f, 0.f, 0.f
#define COLOR_WHITE 	1.f, 1.f, 1.f
#define COLOR_LIGHTBLUE	0.3f, 0.3f, 1.0f

#define VERT_SHADER "shader.vert"
#define FRAG_SHADER "shader.frag"

void loadShader(const char* vertShader, const char* fragShader, GLuint* program);
void checkShader(const char* what, GLint* shader);
bool initGL(void);

GLuint loadTexture(const char *filePath);

static const GLfloat triangleData[] = {
	-1.0f, -1.0f, 0.0f,
	 1.0f, -1.0f, 0.0f,
	 0.0f,  1.0f, 0.0f
};


#endif