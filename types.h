#ifndef TYPES_H
#define TYPES_H

#define true 1
#define false 0

typedef char bool;

typedef struct {
	float x;
	float y;
	float z;
} Vec3f;

Vec3f newVec3f(float x, float y, float z);

#endif