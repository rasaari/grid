#version 120

uniform float time;
attribute vec4 coord;
varying vec4 tex;

void main(void)
{
	gl_FrontColor = gl_Color;
	

	vec4 v = vec4(gl_Vertex);
	tex = v;
	v.w = 1.0;
	vec4 csPosition = gl_ModelViewMatrix * gl_Vertex;

	gl_Position = gl_ModelViewProjectionMatrix * v;

}
