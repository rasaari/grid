#version 120

uniform float time;
uniform sampler2D Difftexture0;
uniform sampler2D Difftexture1;
varying vec2 texcoord;
varying vec2 v_blurTexCoords[14];

vec4 blur(){
    vec4 blur = vec4(0.0);
    blur += texture2D(Difftexture0, v_blurTexCoords[0])*0.0044299121055113265;
    blur += texture2D(Difftexture0, v_blurTexCoords[1])*0.00895781211794;
    blur += texture2D(Difftexture0, v_blurTexCoords[2])*0.0215963866053;
    blur += texture2D(Difftexture0, v_blurTexCoords[3])*0.0443683338718;
    blur += texture2D(Difftexture0, v_blurTexCoords[4])*0.0776744219933;
    blur += texture2D(Difftexture0, v_blurTexCoords[5])*0.115876621105;
    blur += texture2D(Difftexture0, v_blurTexCoords[6])*0.147308056121;
    blur += texture2D(Difftexture0, texcoord) * 0.159576912161;
    blur += texture2D(Difftexture0, v_blurTexCoords[7])*0.147308056121;
    blur += texture2D(Difftexture0, v_blurTexCoords[8])*0.115876621105;
    blur += texture2D(Difftexture0, v_blurTexCoords[9])*0.0776744219933;
    blur += texture2D(Difftexture0, v_blurTexCoords[10])*0.0443683338718;
    blur += texture2D(Difftexture0, v_blurTexCoords[11])*0.0215963866053;
    blur += texture2D(Difftexture0, v_blurTexCoords[12])*0.00895781211794;
    blur += texture2D(Difftexture0, v_blurTexCoords[13])*0.0044299121055113265;
    return blur;
}

float linearizeDepth(vec2 uv){
    float n = 0.1; // camera z near
    float f = 100.0; // camera z far
    float z = texture2D(Difftexture1, uv).x;
    return (2.0 * n) / (f + n - z * (f - n));
}

void main(void){
        vec2 tcoord = texcoord;
        float d = linearizeDepth(tcoord);
        d = 1-d;
        d = (d<0.1)?1:d;
        //gl_FragColor = vec4(d,d,d,1);
        //return;
        //if(tcoord.x > 0.5)
        //    tcoord += 0.05;
        float fx = gl_FragCoord.x/800;
        float mul = tcoord.x-0.5;
        fx = (fx >= 0.5)?1.0-fx:fx;
        tcoord.x += sin(time/500+gl_FragCoord.y/300)*mul*fx*d;
        vec4 color = texture2D(Difftexture0, tcoord);

        gl_FragColor = color;
}