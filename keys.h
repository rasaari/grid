#ifndef KEYS_H
#define KEYS_H

#include "types.h"
#include <SDL/SDL.h>

//available key constants in game
//movement
#define KEY_UP 		(1 << 0)
#define KEY_DOWN	(1 << 1)
#define KEY_LEFT 	(1 << 2)
#define KEY_RIGHT 	(1 << 3)

//action
#define KEY_A		(1 << 4)
#define KEY_B 		(1 << 5)

//general
#define KEY_OK 		(1 << 6)
#define KEY_CANCEL	(1 << 7)

typedef char KeyState;

KeyState _keys;

//update keystate using sdl event
void updateKeys(SDL_Event*);

//return true if spesific key pressed
bool keyDown(char);

#endif