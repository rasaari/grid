#ifndef WORLD_H
#define WORLD_H

#define WORLD_SIZE 8

#include "chunk.h"

typedef struct {
	Chunk chunks[WORLD_SIZE][WORLD_SIZE][WORLD_SIZE];
} World;

void initWorld(World* world, GLuint texture);
void destroyWorld(World* world);

char getWorldBlock(World* world, int x, int y, int z);
void setWorldBlock(World* world, int x, int y, int z, char data);

void renderWorld(World* world);

float getUsage(World* world);

#endif