#include <stdio.h>
#include <stdlib.h>
#include "filereader.h"

char* readFile(const char *filename) {
	char* buffer = NULL;
	int fileSize, readSize;

	FILE *handler = fopen(filename, "r");

	if(handler) {
		fseek(handler, 0, SEEK_END);
		fileSize = ftell(handler);
		rewind(handler);

		buffer = (char*)malloc(sizeof(char)*(fileSize+1));

		readSize = fread(buffer, sizeof(char), fileSize, handler);

		buffer[fileSize] = '\0';

		if(fileSize != readSize) {
			free(buffer);
			buffer = NULL;
			printf("couldn't read file: %s\n", filename);
		}
	}

	fclose(handler);
	return buffer;
}