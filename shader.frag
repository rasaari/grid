#version 120

varying vec4 tex;

uniform sampler2D diffTexture0;

void main(void)
{
		vec2 texcoord; // = vec2((fract(tex.x + tex.z) + tex.w-1) / 15.0, tex.y);

  		if(tex.w < 0)
    		texcoord = vec2((fract(tex.x) + tex.w+16) / 16.0, fract(tex.z));
  		else
    		texcoord = vec2((fract(tex.x + tex.z) + tex.w) / 16.0, fract(tex.y));

		//vec4 wColor = vec4(texW / 16, cos(texW), sin(texW), 1.0);

		//gl_FragColor = gl_Color;
		// gl_FragColor = vec4(1.0,1.0,1.0,1.0);

		vec4 color;

		float texVal = tex.w;

		if(texVal < 0) texVal += 16;

		if(texVal > 14.5)
			color.x = color.y = color.z = 1.0;
		else
			color.x = color.y = color.z = gl_FragCoord.w*4;

		//color *= wColor;
		color *= texture2D(diffTexture0, texcoord);

		//color = clamp(color, 0.02, 0.9);
		gl_FragColor = color;

}