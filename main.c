#include <GL/glew.h>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glew.h>
#include <stdio.h>
#include <math.h>

#include "gl.h"
#include "types.h"
#include "keys.h"

#include "primitives.h"
#include "world.h"
#include "chunk.h"
#include <time.h>

#define PI 3.14159265

bool init(void);
void render(void);
void renderFBO(void);
void renderVBO(void);

// shader program id
GLuint shader;
GLuint fboShader;

GLuint shaderTime;
GLuint fboShaderTime;

GLuint texture0Location;
GLuint texture0;

//framebuffer object
GLuint fbo;
GLuint depthbuffer;

GLuint fboTexture, depthTexture;

float deltaTime = 1.0f;

World world;

char title[200];

Vec3f cameraPosition;
Vec3f cameraRotation;
Vec3f cameraMoveDelta;

int main(int argc, char **argv) {
	srand(time(NULL));
	bool run = true;

	if(init() == false) {
		printf("gl failed\n");
		return 0;
	}

	loadShader(VERT_SHADER, FRAG_SHADER, &shader);
	loadShader("quad.vert", "quad.frag", &fboShader);
	shaderTime = glGetUniformLocation(shader, "time");
	fboShaderTime = glGetUniformLocation(fboShader, "time");
	printf("timeloc: %d\n", fboShaderTime);
	glUseProgram(shader);
	texture0Location = glGetUniformLocation(shader, "diffTexture0");
	glUniform1i(texture0Location, 0);
	glUseProgram(0);

	glUseProgram(fboShader);
	texture0Location = glGetUniformLocation(fboShader, "Difftexture0");
	glUniform1i(texture0Location, 0);
	texture0Location = glGetUniformLocation(fboShader, "Difftexture1");
	glUniform1i(texture0Location, 1);
	glUseProgram(0);

	initWorld(&world, texture0);

	int x, y, z;

	for(y = 0; y < 2; y++) {
		for(x = 0; x < WORLD_SIZE*CHUNK_SIZE; x++) {
			for(z = 0; z < WORLD_SIZE*CHUNK_SIZE; z++)
				setWorldBlock(&world, x, y, z, 1);
		}
	}


	for(y = 2; y < CHUNK_SIZE; y++) {
		for(x = 0; x < WORLD_SIZE*CHUNK_SIZE; x++) {
			for(z = 0; z < WORLD_SIZE*CHUNK_SIZE; z++)
				if(x%20 > 10 && z%20 > 10)
					setWorldBlock(&world, x, y, z, rand()%16);
		}
	}

	glBindAttribLocation(shader, 0, "coord");
	glBindAttribLocation(fboShader, 0, "coord");

	SDL_Event event;

	int frames = 0;
	int lastTime = SDL_GetTicks();
	float lastFrame = SDL_GetTicks();
	while(run) {
		if(SDL_GetTicks() - lastTime > 1000) {
			SDL_WM_SetCaption(title, NULL);

			deltaTime = 60.0f/frames;

			sprintf(title, "FPS: %d -- %.2f mb -- delta: %.2f", frames, getUsage(&world)/1024, deltaTime);

			lastTime = SDL_GetTicks();
			frames = 0;
		}

		frames++;


		int rx = rand()%(CHUNK_SIZE*WORLD_SIZE);
		int ry = rand()%(CHUNK_SIZE*WORLD_SIZE);
		int rz = rand()%(CHUNK_SIZE*WORLD_SIZE);


		setWorldBlock(&world, rx, ry, rz, rand()%16);


		while(SDL_PollEvent(&event)) {
			if(event.type == SDL_QUIT)
				run = false;

			updateKeys(&event);

			// update camera rotation
			if(event.type == SDL_MOUSEMOTION) {
				cameraRotation.x += event.motion.xrel*0.5;
				cameraRotation.y += event.motion.yrel*0.5;
				cameraRotation.z = 0.0f;

				if(cameraRotation.y < -80.0f)
					cameraRotation.y = -80.0f;

				if(cameraRotation.y > 80.0f)
					cameraRotation.y = 80.0f;
			}
		}

		// calculate camera movement delta
		cameraMoveDelta.z = -cosf(cameraRotation.x * PI / 180)*0.2*deltaTime;
		cameraMoveDelta.x = sinf(cameraRotation.x * PI / 180)*0.2*deltaTime;

		if(keyDown(KEY_CANCEL))
			run = false;

		if(keyDown(KEY_UP)) {
			cameraPosition.z -= cameraMoveDelta.z;
			cameraPosition.x -= cameraMoveDelta.x;
		}

		if(keyDown(KEY_DOWN)) {
			cameraPosition.z += cameraMoveDelta.z;
			cameraPosition.x += cameraMoveDelta.x;
		}

		if(keyDown(KEY_LEFT)){
			cameraPosition.x += -cameraMoveDelta.z;
			cameraPosition.z += cameraMoveDelta.x;
		}

		if(keyDown(KEY_RIGHT)){
			cameraPosition.x -= -cameraMoveDelta.z;
			cameraPosition.z -= cameraMoveDelta.x;
		}

		if(keyDown(KEY_A))
			cameraPosition.y -= 0.2f*deltaTime;

		if(keyDown(KEY_B))
			cameraPosition.y += 0.2f*deltaTime;

		if(cameraRotation.y < -45) {
			float altSky = -(cameraRotation.y+48)/80.0f;
			glClearColor(altSky/2, altSky/2, altSky, 1.0f);
		}

		else {
			glClearColor(COLOR_BLACK, 1.0f);
		}

		// disable mouse motion capturing while moving
		// cursor to the center of the window
		SDL_EventState(SDL_MOUSEMOTION, SDL_IGNORE);
		SDL_WarpMouse(SCREEN_WIDTH>>1, SCREEN_HEIGHT>>1);
		SDL_EventState(SDL_MOUSEMOTION, SDL_ENABLE);

		//cullChunks();
		render();
	}

	return 0;
}

bool init(void) {
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
		return false;

	if(SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_OPENGL) < 0)
		return false;

	if(!initGL())
		return false;

	glGenFramebuffersEXT(1, &fbo);
	glGenTextures(1, &fboTexture);
	glGenTextures(1, &depthTexture);

	glBindTexture(GL_TEXTURE_2D, fboTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);

	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, fboTexture, 0);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, depthTexture, 0);

	GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
	if(status != GL_FRAMEBUFFER_COMPLETE_EXT)
		printf("fbo failure!\n");

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	// disable mouse motion capturing while grapping input
	// and re-enable after that.
	SDL_EventState(SDL_MOUSEMOTION, SDL_IGNORE);
	//SDL_WM_GrabInput( SDL_GRAB_ON );
	SDL_EventState(SDL_MOUSEMOTION, SDL_ENABLE);

	// Hide cursor
	SDL_ShowCursor(SDL_DISABLE);

	SDL_WM_SetCaption("Grid", NULL);

	// set initial camera position and rotation
	cameraPosition.x = -60.0f;
	cameraPosition.y = -8.0f;
	cameraPosition.z = -50.0f;

	cameraRotation.x = 0.0f;
	cameraRotation.y = 1.0f;
		cameraRotation.z = 0.0f;

	texture0 = loadTexture("texture.png");

	return true;
}

void render(void) {
	renderFBO();

	glViewport(0,0,SCREEN_WIDTH, SCREEN_HEIGHT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0.1f, 10.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(fboShader);
	glUniform1f(fboShaderTime, (float)SDL_GetTicks());

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, fboTexture);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, depthTexture);

	glTranslatef(0.0f, 0.0f, -8.0f);

	glBegin(GL_QUADS);
		glTexCoord2f(0,1); glVertex3f(0, 0, 0.0f);
		glTexCoord2f(0,0); glVertex3f(0, SCREEN_HEIGHT, 0.0f);
		glTexCoord2f(1,0); glVertex3f(SCREEN_WIDTH, SCREEN_HEIGHT, 0.0f);
		glTexCoord2f(1,1); glVertex3f(SCREEN_WIDTH, 0, 0.0f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glUseProgram(0);

	SDL_GL_SwapBuffers();
}

void renderFBO(void) {
	glViewport(0,0,SCREEN_WIDTH, SCREEN_HEIGHT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(VIEW_ANGLE,(GLfloat)SCREEN_WIDTH/(GLfloat)SCREEN_HEIGHT,0.1f,500.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(shader);

	glUniform1f(shaderTime, (float)SDL_GetTicks());

	glRotatef(cameraRotation.y, 1.0f, 0.0f, 0.0f);
	glRotatef(cameraRotation.x, 0.0f, 1.0f, 0.0f);
	glTranslatef(cameraPosition.x, cameraPosition.y, cameraPosition.z);

	glPushMatrix();
	renderWorld(&world);
	glPopMatrix();

	glUseProgram(0);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}

void renderVBO(void) {
	glEnableVertexAttribArray(0);
	//glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(
		0,
		4,
		GL_BYTE,
		GL_FALSE,
		0,
		(void*)0
	);

	glDrawArrays(GL_TRIANGLES, 0, 3);
	glDisableVertexAttribArray(0);
}