#include <stdlib.h>
#include <stdio.h>
#include "gl.h"
#include "filereader.h"

bool initGL() {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(VIEW_ANGLE,(GLfloat)SCREEN_WIDTH/(GLfloat)SCREEN_HEIGHT,0.1f,500.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glClearColor(COLOR_BLACK, 1.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_FLAT);
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);

	glEnable(GL_TEXTURE_2D);

	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
		return false;

	glewInit();

	if(!glewIsSupported("GL_VERSION_2_0"))
		return false;

	return true;
}

void loadShader(const char* vertShader, const char* fragShader, GLuint* program) {
	char *vs, *fs;
	GLuint v, f;
	v = glCreateShader(GL_VERTEX_SHADER);
	f = glCreateShader(GL_FRAGMENT_SHADER);

	vs = readFile(vertShader);
	fs = readFile(fragShader);

	const char* vv = vs;
	const char* ff = fs;

	glShaderSource(v, 1, &vv, NULL);
	glShaderSource(f, 1, &ff, NULL);

	free(vs);
	free(fs);

	glCompileShader(v);
	glCompileShader(f);

	checkShader("vertex shader", &v);
	checkShader("fragment shader",&f);

	*program = glCreateProgram();

	glAttachShader(*program, v);
	glAttachShader(*program, f);

	glLinkProgram(*program);

	//glUseProgram(*program);
}

void checkShader(const char* what, GLint* shader) {
	GLint success = 0;
	glGetShaderiv(*shader, GL_COMPILE_STATUS, &success);

	if(success == GL_FALSE) {
		GLint maxLen = 0;
		glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &maxLen);

		GLchar log[maxLen+1];
		glGetShaderInfoLog(*shader, maxLen, NULL, log);

		printf("error in %s: %s\n", what, log);
	}
}

GLuint loadTexture(const char *filePath){
	SDL_Surface *img = IMG_Load(filePath);
	GLuint texture;
	
	if(img){
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		glTexImage2D(GL_TEXTURE_2D, 0, img->format->BytesPerPixel, img->w, img->h, 0, (img->format->BytesPerPixel == 3) ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, img->pixels);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		SDL_FreeSurface(img);
		glBindTexture(GL_TEXTURE_2D, 0);
		
		return texture;
	}
	else{
		printf("Failed to load texture\n");
		return 0;
	}

}