#ifndef VOXEL_H
#define VOXEL_H

#include <GL/gl.h>
#include "types.h"

//block flags
#define BLOCK_ENABLED (1 << 0)

//size equals width, height or depth
#define CHUNK_SIZE 16
#define BLOCKS3 16*16*16

typedef int* BlockArray;
typedef int BlockType;

typedef struct {
	GLbyte x, y, z;
	GLubyte type;
} Vertex;

typedef struct {
	char blocks[CHUNK_SIZE][CHUNK_SIZE][CHUNK_SIZE];
	GLuint vbo;
	GLuint attributeCoord;
	GLuint texture;

	int vertexCount;
	bool changed;
} Chunk;

void initChunk(Chunk*);
void destroyChunk(Chunk*);

char getChunkBlock(Chunk*, int x, int y, int z);
void setChunkBlock(Chunk* chunk, int x, int y, int z,  char data);

void updateChunk(Chunk* chunk);
void renderChunk(Chunk* chunk);

#endif