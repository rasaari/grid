#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "chunk.h"


void initChunk(Chunk* chunk) {
	memset(chunk->blocks, 0, sizeof(chunk->blocks));

	glGenBuffers(1, &chunk->vbo);
	chunk->changed = true;
	chunk->attributeCoord = 0;
}

void destroyChunk(Chunk* chunk) {
	glDeleteBuffers(1, &chunk->vbo);
}

char getChunkBlock(Chunk* chunk, int x, int y, int z) {
	return chunk->blocks[y][x][z];
}

void setChunkBlock(Chunk* chunk, int x, int y, int z, char data) {
	if(chunk->blocks[y][x][z] == data)
		return;

	chunk->blocks[y][x][z] = data;

	chunk->changed = true;
}

void updateChunk(Chunk* chunk) {
	chunk->changed = false;

	// v as in vertex
	Vertex v[CHUNK_SIZE*CHUNK_SIZE*CHUNK_SIZE*6*6];

	int i = 0, x, y, z;

	for(y = 0; y < CHUNK_SIZE; y++) {
		for(x = 0; x < CHUNK_SIZE; x++) {
			for(z = 0; z < CHUNK_SIZE; z++) {
				if(chunk->blocks[y][x][z] <= 0)
					continue;

				// --front
				if(z < CHUNK_SIZE-1 && chunk->blocks[y][x][z+1] > 0) { /* do nothing */ }
				else {
				v[i].x = x;		v[i].y = y; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x+1;	v[i].y = y; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x+1;	v[i].y = y+1; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z];

				v[i].x = x+1;	v[i].y = y+1; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x;		v[i].y = y+1; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x;		v[i].y = y; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z];
				}

				// --back
				if(z > 0 && chunk->blocks[y][x][z-1] > 0) { /* do nothing */ }
				else {
				v[i].x = x+1;	v[i].y = y; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x;		v[i].y = y; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x;		v[i].y = y+1; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z];

				v[i].x = x;		v[i].y = y+1; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x+1;	v[i].y = y+1; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x+1;	v[i].y = y; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z];
				}

				// --left
				if(x > 0 && chunk->blocks[y][x-1][z] > 0) { /* do nothing */ }
				else {
				v[i].x = x;		v[i].y = y; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x;		v[i].y = y; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x;		v[i].y = y+1; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z];

				v[i].x = x;		v[i].y = y+1; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x;		v[i].y = y+1; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x;		v[i].y = y; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z];
				}

				// --right
				if(x < CHUNK_SIZE-1 && chunk->blocks[y][x+1][z] > 0) { /* do nothing */ }
				else {
				v[i].x = x+1;	v[i].y = y; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x+1;	v[i].y = y; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x+1;	v[i].y = y+1; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z];

				v[i].x = x+1;	v[i].y = y+1; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x+1;	v[i].y = y+1; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z];
				v[i].x = x+1;	v[i].y = y; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z];
				}

				// bottom
				if(y > 0 && chunk->blocks[y-1][x][z] > 0) { /* do nothing */ }
				else {
				v[i].x = x;		v[i].y = y; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z]-16;
				v[i].x = x+1;	v[i].y = y; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z]-16;
				v[i].x = x+1;	v[i].y = y; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z]-16;

				v[i].x = x+1;	v[i].y = y; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z]-16;
				v[i].x = x;		v[i].y = y; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z]-16;
				v[i].x = x;		v[i].y = y; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z]-16;
				}

				// top
				if(y < CHUNK_SIZE-1 && chunk->blocks[y+1][x][z] > 0) { /* do nothing */ }
				else {
				v[i].x = x;		v[i].y = y+1; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z]-16;
				v[i].x = x;		v[i].y = y+1; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z]-16;
				v[i].x = x+1;	v[i].y = y+1; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z]-16;

				v[i].x = x+1;	v[i].y = y+1; 	v[i].z = z+1; 	v[i++].type = chunk->blocks[y][x][z]-16;
				v[i].x = x+1;	v[i].y = y+1; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z]-16;
				v[i].x = x;		v[i].y = y+1; 	v[i].z = z; 	v[i++].type = chunk->blocks[y][x][z]-16;
				}
			}
		}
	}

	chunk->vertexCount = i;

	int len = sizeof(Vertex)*i;
	char * raw = malloc(len);
	memcpy(raw, v, len);

	int j;
	for(j = 0; j < len; j += 4) {
		//printf("%d %d %d %d \n", raw[j], raw[j+1], raw[j+2], raw[j+3]);
	}

	glBindBuffer(GL_ARRAY_BUFFER, chunk->vbo);
	glBufferData(GL_ARRAY_BUFFER, len, raw, GL_STATIC_DRAW);

	free(raw);
}

void renderChunk(Chunk* chunk) {
	if(chunk->changed)
		updateChunk(chunk);

	//if nothing to draw, don't even try to.
	if(chunk->vertexCount == 0)
		return;

	//glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glEnable(GL_DEPTH_TEST);

	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, chunk->vbo);
	glBindTexture(GL_TEXTURE_2D, chunk->texture);
	glVertexAttribPointer(chunk->attributeCoord, 4, GL_BYTE, GL_FALSE, 0, 0);
	glDrawArrays(GL_TRIANGLES, 0, chunk->vertexCount);
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisableVertexAttribArray(0);
}

/*
BlockType* blockAt(Chunk* chunk, int x, int y, int z) {
	return &chunk->blocks[x+(y*CHUNK_SIZE) + (z*CHUNK_SIZE*CHUNK_SIZE)];
}

void setBlock(World* world, int x, int y, int z, BlockType data) {
	int cx = (x - x % CHUNK_SIZE)/CHUNK_SIZE;
	int cy = (y - y % CHUNK_SIZE)/CHUNK_SIZE;
	int cz = (z - z % CHUNK_SIZE)/CHUNK_SIZE;

	Chunk* chunk = chunkAt(world, cx, cy, cz);

	if(chunk == NULL)
		printf("null chunk!\n");

	BlockType* block = blockAt(
							chunk,
							x-cx*CHUNK_SIZE,
							y-cy*CHUNK_SIZE,
							z-cz*CHUNK_SIZE);

	*block = data;
}

BlockType* getBlock(World* world, int x, int y, int z) {
	int cx = (x - x % CHUNK_SIZE)/CHUNK_SIZE;
	int cy = (y - y % CHUNK_SIZE)/CHUNK_SIZE;
	int cz = (z - z % CHUNK_SIZE)/CHUNK_SIZE;

	Chunk* chunk = chunkAt(world, cx, cy, cz);

	BlockType* block = blockAt(
							chunk,
							x-cx*CHUNK_SIZE,
							y-cy*CHUNK_SIZE,
							z-cz*CHUNK_SIZE);

	return block;
}

//-------------------------------

void initWorld(World* world, int width, int height, int depth) {
	world->chunks = (ChunkArray)malloc(sizeof(BlockArray)*width*height*depth);
	world->width = width;
	world->height = height;
	world->depth = depth;

	world->chunkSizes = (int*)malloc(sizeof(int)*width*height*depth);

	int index;
	for(index = 0; index < width*height*depth; index++) {
		world->chunkSizes[index] = 0;
	}

	for(index = 0; index < width*height*depth; index++) {
		initChunk((Chunk*)&world->chunks[index]);
	}
	//
}

void destroyWorld(World* world) {
	int index = 0;
	for(index; index < (world->width)*(world->height)*(world->depth); index++) {
		destroyChunk((Chunk*)&world->chunks[index]);
	}

	free(world->chunks);
}

Chunk* chunkAt(World* world, int x, int y, int z) {
	return (Chunk*)&world->chunks[x+(y*world->width) + (z*world->width*world->height)];
}

//-------------------------------

void buildVBOData(World* world, long int* size, GLfloat** array) {
	int wx, wy, wz;

	int blockCount = (world->height)*(world->width)*(world->depth)*CHUNK_SIZE*CHUNK_SIZE*CHUNK_SIZE;
	int floats = blockCount*6*2*3*3;
	*size = floats;
	*array = (GLfloat*)malloc(sizeof(GLfloat)*floats);

	int blockSkip = 0;

	int chunkId = 0;
	int totalTriangles;
	for(wy = 0; wy < world->height; wy++) {
		for(wx = 0; wx < world->width; wx++) {
			for(wz = 0; wz < world->depth; wz++) {
				Chunk* c = chunkAt(world, wx, wy, wz);
				world->chunkSizes[chunkId] = buildChunkVBO(c, array, wx, wy, wz, chunkId*CHUNK_SIZE*CHUNK_SIZE*CHUNK_SIZE*6*2*3*3);
				totalTriangles += world->chunkSizes[chunkId++];
			}
		}
	}

	printf("triangles used: %d\n", totalTriangles/3/3);
}

int buildChunkVBO(Chunk* c, GLfloat** array, int wx, int wy, int wz, int i) {
	int cx, cy, cz, blockSkip = 0, quadSkip = 0;
	for(cy = 0; cy < CHUNK_SIZE; cy++) {
		for(cx = 0; cx < CHUNK_SIZE; cx++) {
			for(cz = 0; cz < CHUNK_SIZE; cz++) {
				BlockType* block = blockAt(c, cx, cy, cz);
				if((*block & BLOCK_ENABLED) == false && (*block & BLOCK_VISIBLE) == false) {
					blockSkip++;
					continue;
				}

				if(cx > 0 && *blockAt(c, cx-1, cy, cz) & BLOCK_ENABLED) {
					quadSkip++;
				}
				else {
					//-- left
					(*array)[i++] = -0.5+cx+wx*CHUNK_SIZE; (*array)[i++] = -0.5+cy+wy*CHUNK_SIZE; (*array)[i++] =-0.5+cz+wz*CHUNK_SIZE;
					(*array)[i++] = -0.5+cx+wx*CHUNK_SIZE; (*array)[i++] = -0.5+cy+wy*CHUNK_SIZE; (*array)[i++] = 0.5+cz+wz*CHUNK_SIZE;
					(*array)[i++] = -0.5+cx+wx*CHUNK_SIZE; (*array)[i++] =  0.5+cy+wy*CHUNK_SIZE; (*array)[i++] = 0.5+cz+wz*CHUNK_SIZE;

					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
				}

				if(cz > 0 && *blockAt(c, cx, cy, cz-1) & BLOCK_ENABLED) {
					quadSkip++;
				}
				else {
					//-- back
					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);

					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
				}

				if(cy > 0 && *blockAt(c, cx, cy-1, cz) & BLOCK_ENABLED) {
					quadSkip++;
				}
				else {
					//-- bottom
					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);

					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
				}


				if(cz < CHUNK_SIZE-1 && *blockAt(c, cx, cy, cz+1) & BLOCK_ENABLED) {
					quadSkip++;
				}
				else {
					//-- front
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);

					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);
				}

				if(cx < CHUNK_SIZE-1 && *blockAt(c, cx+1, cy, cz) & BLOCK_ENABLED) {
					quadSkip++;
				}
				else {
					//-- right
					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);

					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] = -0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
				}

				if(cy < CHUNK_SIZE-1 && *blockAt(c, cx, cy+1, cz) & BLOCK_ENABLED) {
					quadSkip++;
				}
				else {
					//-- top
					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);

					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] =  0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] =  0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
					(*array)[i++] = -0.5+cx+(CHUNK_SIZE*wx); (*array)[i++] =  0.5+cy+(CHUNK_SIZE*wy); (*array)[i++] = -0.5+cz+(CHUNK_SIZE*wz);
				}


			}
		}
	}

	return CHUNK_SIZE*CHUNK_SIZE*CHUNK_SIZE*6*2*3*3-blockSkip*6*2*3*3-quadSkip*2*3*3;
}

//-------------------------------

void generateWorld(World* world) {
	//make floor
	int x, y, z, i;
	BlockType* block;

	for(y = 0; y < 3; y++) {
		for(x = 0; x < world->width*CHUNK_SIZE; x++) {
			for(z = 0; z < world->depth*CHUNK_SIZE; z++) {
				setBlock(world, x, y, z, (BLOCK_ENABLED | BLOCK_VISIBLE));
			}
		}
	}

	while(y < 30) {
		for(x = 0; x < world->width*CHUNK_SIZE; x++) {
			for(z = 0; z < world->depth*CHUNK_SIZE; z++) {
				block = getBlock(world, x, y-1 , z);
				if(  rand()%2 == 1 &&  x%30 > 8 && z%30 > 8  &&  *block == (BLOCK_ENABLED | BLOCK_VISIBLE) ) {
					setBlock(world, x, y, z, (BLOCK_ENABLED | BLOCK_VISIBLE));
				}
			}
		}

		y++;
	}
}
*/