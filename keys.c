#include "keys.h"

void updateKeys(SDL_Event* event) {
	if(event->type == SDL_KEYDOWN) {
		switch(event->key.keysym.sym) {
			case SDLK_w:
				_keys |= KEY_UP;
				break;
			case SDLK_s:
				_keys |= KEY_DOWN;
				break;
			case SDLK_a:
				_keys |= KEY_LEFT;
				break;
			case SDLK_d:
				_keys |= KEY_RIGHT;
				break;
			case SDLK_e:
				_keys |= KEY_A;
				break;
			case SDLK_q:
				_keys |= KEY_B;
				break;
			case SDLK_ESCAPE:
				_keys |= KEY_CANCEL;
				break;
		}
	}

	if(event->type == SDL_KEYUP) {
		switch(event->key.keysym.sym) {
			case SDLK_w:
				_keys &= ~KEY_UP;
				break;
			case SDLK_s:
				_keys &= ~KEY_DOWN;
				break;
			case SDLK_a:
				_keys &= ~KEY_LEFT;
				break;
			case SDLK_d:
				_keys &= ~KEY_RIGHT;
				break;
			case SDLK_e:
				_keys &= ~KEY_A;
				break;
			case SDLK_q:
				_keys &= ~KEY_B;
				break;
			case SDLK_ESCAPE:
				_keys & ~KEY_CANCEL;
				break;
		}
	}
}

bool keyDown(char keycode) {
	return (_keys & keycode);
}