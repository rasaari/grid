#include "world.h"

void initWorld(World* world, GLuint texture) {
	int x, y, z;
	for(y = 0; y < WORLD_SIZE; y++) {
		for(x = 0; x < WORLD_SIZE; x++) {
			for(z = 0; z < WORLD_SIZE; z++) {
				initChunk(&world->chunks[y][x][z]);
				world->chunks[y][x][z].texture = texture;
			}
		}
	}
}

void destroyWorld(World* world) {

}

char getWorldBlock(World* world, int x, int y, int z) {
	int cx = (x - x % CHUNK_SIZE)/CHUNK_SIZE;
	int cy = (y - y % CHUNK_SIZE)/CHUNK_SIZE;
	int cz = (z - z % CHUNK_SIZE)/CHUNK_SIZE;

	Chunk* chunk = &world->chunks[cy][cx][cz];

	return getChunkBlock(chunk, y-cy*CHUNK_SIZE, x-cx*CHUNK_SIZE, z-cz*CHUNK_SIZE);
}

void setWorldBlock(World* world, int x, int y, int z, char data) {
	int cx = (x - x % CHUNK_SIZE)/CHUNK_SIZE;
	int cy = (y - y % CHUNK_SIZE)/CHUNK_SIZE;
	int cz = (z - z % CHUNK_SIZE)/CHUNK_SIZE;

	Chunk* chunk = &world->chunks[cy][cx][cz];

	setChunkBlock(chunk, x-cx*CHUNK_SIZE, y-cy*CHUNK_SIZE, z-cz*CHUNK_SIZE, data);
}

void renderWorld(World* world) {
	int x, y, z;
	for(y = 0; y < WORLD_SIZE; y++) {
		for(x = 0; x < WORLD_SIZE; x++) {
			for(z = 0; z < WORLD_SIZE; z++) {
				glPushMatrix();
				glTranslatef(x*CHUNK_SIZE, y*CHUNK_SIZE, z*CHUNK_SIZE);
				renderChunk(&world->chunks[y][x][z]);
				glPopMatrix();
			}
		}
	}
}

float getUsage(World* world) {
	float usage = 0.0f;

	int x, y, z;
	for(y = 0; y < WORLD_SIZE; y++) {
		for(x = 0; x < WORLD_SIZE; x++) {
			for(z = 0; z < WORLD_SIZE; z++) {
				usage += world->chunks[y][x][z].vertexCount*4;
			}
		}
	}

	return usage/1024;
}